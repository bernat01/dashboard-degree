
use employees;

--
-- Table structure for table `users`
--
CREATE TABLE dashboard_users (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `email` varchar(155) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `logins` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--
INSERT INTO dashboard_users (`email`, `username`, `password`, `fname`, `lname`, `logins`) VALUES
('bernatgalmesrules@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'User', 0),
('dd@sdf.com', 'admin', 'admin', 'Admin', 'User', 0);