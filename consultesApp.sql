SELECT 
    gender, AVG(gender)
FROM
    employees.employees
GROUP BY gender;

SELECT 
    departments.dept_name, COUNT(*)
FROM
    dept_emp INNER JOIN departments
    ON dept_emp.dept_no = departments.dept_no
GROUP BY departments.dept_no;

SELECT 
	YEAR(employees.birth_date), COUNT(*)
FROM
	employees
GROUP BY YEAR(employees.birth_date);

SELECT
	AVG(salaries.salary), YEAR(employees.birth_date)
FROM
	employees INNER JOIN salaries
    ON employees.emp_no = salaries.emp_no
GROUP BY YEAR(employees.birth_date);

SELECT
	AVG(salaries.salary), YEAR(salaries.from_date)
FROM
	salaries
GROUP BY YEAR(salaries.from_date);

SELECT
	* -- salary, YEAR(salaries.from_date)
FROM
	salaries
WHERE YEAR(from_date) <= 1990 AND YEAR(to_date) >= 1990;

SELECT 
    COUNT(*)
FROM
    employees
        INNER JOIN
    dept_emp ON dept_emp.emp_no = employees.emp_no
WHERE
    dept_emp.dept_no = d005;
SELECT 
    COUNT(*)
FROM
    employees INNER JOIN
    dept_emp ON dept_emp.emp_no = employees.emp_no
WHERE
    dept_emp.dept_no = "d005"
GROUP BY gender;
    
