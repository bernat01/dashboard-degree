<%-- 
    Document   : mainPage
    Created on : 07-ene-2017, 11:58:21
    Author     : bernat
--%>

<%@page import="Dashboard.Login"%>
<jsp:useBean id="login" class="Dashboard.Login" scope="session" />
<% login.blindPage(response);%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="nofollow">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <title>Dashboard Employees</title>
    </head>
    <body>     
        <!-- Navbar -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" id="logoEmpresa"><i class="fa fa-universal-access" aria-hidden="true"></i> DashboardEmployees</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i> <span class="badge">1</span> <span class="caret"></span>
                            </a>
                            <ul id="messagesNavbar" class="dropdown-menu navbar-item-lista">
                                <li>
                                    <a href="mailto:CEO@webapping.com">
                                        <div>
                                            <b>Investigación: </b> <small class="pull-right">14 minutes ago</small>
                                        </div>
                                        <div class="navbar-item-messages">
                                            Falta de presupuesto en el departamento.
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell" aria-hidden="true"></i> <span id="notificationBadge" class="badge">2</span> <span class="caret"></span>
                            </a>
                            <ul id="notificationsNavbar" class="dropdown-menu navbar-item-lista">
                                <li>
                                    <a>
                                        <div> <i class="fa fa-money"></i> Se ha actualizado el salario de Miguel Suárez Orozco.</div>
                                    </a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a>
                                        <div> <i class="fa fa-users"></i> Nuevo trabajador: Miguel Cuenca Alfaguara.</div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="logout.jsp"><i class="fa fa-key" aria-hidden="true"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END Navbar -->

        <div class="container-fluid">
            <div class="row">
                <!-- Left Menu -->
                <aside>
                    <div class="col-sm-3 col-md-2 sidebar">
                        <div>
                            <img src="img/image1.png" id="imagenUsuario" class="imgInline" src="" width="70px" height="70px">
                            <h5 class="imgInline">Bienvenido,<br> <span id="nombreUsuario"></span></h5>
                        </div>
                        <hr>
                        <ul class="nav nav-sidebar">
                            <li id="btnDepts" class="menuItem active"><a href="#"><i class="fa fa-file-text" aria-hidden="true"></i> Departamentos</a></li>
                            <li id="btnEmployees" class="menuItem"><a href="#"><i class="fa fa-users"></i> Empleados</a></li>
                            <li id="btnSalaris" class="menuItem"><a href="#"><i class="fa fa-money"></i> Salarios</a></li>
                        </ul>
                    </div>
                </aside>
                <!-- END Left Menu -->

                <!-- Content -->
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <div class="page-header">
                        <h1 id="sectName"></h1>
                    </div>
                    <section class='row'>
                        <div class="col-md-2" align="center">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Total empleados</div>
                                <div id="nEmpl" class="panel-body"></div>
                            </div>
                        </div>
                        <div id="panelPrincipal" class="col-md-10" align="center"></div>
                    </section>
                </div>
                <!-- END Content -->

                <!-- Modal to give feedback -->
                <div id="modalFeedback" class="modal fade feed-back-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog"><div class="modal-content"> <div class="modal-body"><h5 id="feedbackMessage"></h5></div></div></div>
                </div>
                <div id="modalFeedbackError" class="modal fade feed-back-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog"><div class="modal-content"> <div class="modal-body"><h5 id="feedbackMessageError"></h5></div></div></div>
                </div>
                <!-- END Modal -->
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/drilldown.js"></script>
        <script src="https://code.highcharts.com/modules/heatmap.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script type="text/javascript" src="js/WS_access.js"></script>
        <script type="text/javascript" src="js/dashboard.js"></script>
        <script type="text/javascript" src="js/HG_wrapper.js"></script>
        <!-- <script type="text/javascript" src="js/charts/highcharts.js"></script>
         <script type="text/javascript" src="js/charts/modules/exporting.js"></script>-->
        <!--<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>-->
    </body>
</html>