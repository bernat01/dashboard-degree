<%-- 
    Document   : employees
    Created on : 07-ene-2017, 16:13:10
    Author     : bernat
--%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="aBD" scope="request" class="adiiubd.AccesoABD"></jsp:useBean>
    <style type="text/css">
    ${demo.css}
</style>

<script type="text/javascript">
    //Datos para el grafico con los trabajadores por a�o de nacimiento
    var categories = [
    <%
        ArrayList<int[]> res = aBD.getNumEmpByAge();
        for (int i = 0; i < res.size(); i++) {
            out.print("'" + res.get(i)[0] + "',");
        }
    %>
    ];
    
    var data = [{
    name: 'Numero de trabajadores',
            data: [
    <%
        for (int i = 0; i < res.size(); i++) {
            out.print(res.get(i)[1] + ",");
        }
    %>
            ]
    }];        
</script>
<div class="row">
    <div class="col-md-6">
        <div class="form-group ">
            <label for="selEmp">Selecciona un trabajador:</label>
            <select id="selEmp" class="col-md-12" style="overflow-y: scroll;" size="20"></select>           
        </div>
    </div>
    <div class="col-md-6">
        <div id="Employee-info">
            <ul class="list-group">
                <h3>Informaci�n trabajador selecionado:</h3>
                <li class="list-group-item"><b>Nombre:</b> <span id="Employee-name"></span></li>
                <li class="list-group-item"><b>Apellidos:</b> <span id="Employee-last_name"></span></li>
                <li class="list-group-item"><b>Fecha nacimiento:</b> <span id="Employee-birth_date"></span></li>
                <li class="list-group-item"><b>Fecha contrataci�n:</b> <span id="Employee-hire_date"></span></li>
            </ul>
        </div>
        <div id="EmployeeChart"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div id="AgeBar"></div>
    </div>
    <div class="col-md-6">
    </div>
</div>
<script type="text/javascript" src="js/empleados.js"></script>