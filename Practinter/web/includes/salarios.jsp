<%-- 
    Document   : salarios
    Created on : 07-ene-2017, 21:03:54
    Author     : bernat
--%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="aBD" scope="request" class="adiiubd.AccesoABD"></jsp:useBean>
    <style type="text/css">
    ${demo.css}
</style>
<script type="text/javascript">
    $(document).ready(function (){
        var categ = [
    <%
                ArrayList<String[]> res = aBD.getAvgSalByAge();
                for (int i = 0; i < res.size(); i++) {
                    out.print("'" + res.get(i)[0] + "',");
                }
    %>
        ];
        var series = [{
                name: 'dolars anuals',
                data: [
    <%
        for (int i = 0; i < res.size(); i++) {
            out.print(res.get(i)[1] + ",");
        }
    %> 
                ]
            }];
        genLine('#Salarios', 'Evolución salario medio', 'dolars anuals', 'dolars($)', categ, series);
    });
</script>
<div id="Salarios"></div>