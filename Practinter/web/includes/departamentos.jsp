<%-- 
    Document   : departamentos
    Created on : 07-ene-2017, 21:02:27
    Author     : bernat
--%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="aBD" scope="request" class="adiiubd.AccesoABD"></jsp:useBean>
<script type="text/javascript">
   var json_data = [
    <%
        ArrayList<String[]> res = aBD.getNumEmpByDept();
        for (int i = 0; i < res.size(); i++) {
            out.print("{name: \"" + res.get(i)[1] + "\",id: '" + res.get(i)[0] + "', ");
            out.print("y: " + res.get(i)[2] + ",drilldown: true},");
        }
    %>
        ];      
    var series = hgc_buidSeriesPie("Cantidad trabajadores", json_data)
</script>
<script type="text/javascript" src="js/departaments.js"></script>
<div class="row">
    <div class="col-md-12">
        <div id="Departments" align="center"></div>
    </div>
</div>

