<%-- 
    Document   : login
    Created on : 07-ene-2017, 11:44:25
    Author     : bernat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="nofollow">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
        <script type="text/javascript" src="js/video.js"></script>
        <title>Employees dashboard</title>
        <script>
            $(document).ready(function () {
                $("#mute").click(function () {
                    if(getController().muted === true){
                        getController().muted = false;
                        $("#iconMute").attr("class",'glyphicon glyphicon-volume-up');
                    }else{
                        getController().muted = true;
                        $("#iconMute").attr("class",'glyphicon glyphicon-volume-off');
                    }
                });
            });
        </script>
    </head>
    <body>
        <jsp:useBean id="login" class="Dashboard.Login" scope="session" />
        <%
            int code = login.doLogin(request, response);
        %>
        <button id="mute" class="btn btn-default"><span id="iconMute" class="glyphicon glyphicon-volume-up"></span></button>
        <section class="container">            
            <video  autoplay loop muted 
                    poster="https://wallpaperscraft.com/image/star_wars_lego_hunt_toys_29186_3840x2160.jpg">
                <source src="media/login.mp4" type="video/mp4">
            </video>
            <div class="login">
                <h1 class="form-signin-heading">Dashboard Login</h1>
                <form action="login.jsp" method="post">  
                    <% if (code == 2) { // el login ha fallat %>
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        Password o usuario incorrecto.
                    </div>
                    <%
                        }
                    %>
                    <p><input type="text" id="IPusername" name="IPusername" class="form-control" placeholder="ID usuario" required autofocus></p>
                    <p><input type="password" id="IPPassword" name="IPPassword" class="form-control" placeholder="Contraseña" required></p>
                    <p class="submit"><input type="submit" name="commit" value="Entrar"></p>
                    <script>
                        $(document).ready(function () {
                            $("#log").focus();
                        });
                    </script>
                </form>

            </div>
            <audio id="video" autoplay>
                <source src="./media/hoc.mp3" type="audio/mpeg">
                Your browser does not support the audio element.
            </audio>
        </section>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>