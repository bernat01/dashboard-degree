<%-- 
    Document   : logout
    Created on : 09-ene-2017, 20:04:34
    Author     : bernat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Empleados</title>   

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="js/WS_access.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="css/public.css">
    </head>
    <body>
        <jsp:useBean id="login" class="Dashboard.Login" scope="session" />
        <%
            login.logout();
            session.invalidate();
        %>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a href="./index.jsp" class="navbar-brand" id="logoEmpresa"><i class="fa fa-universal-access" aria-hidden="true"></i>Employees</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="./login.jsp"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid text-center">    
            <div class="row content">
                <div class="col-sm-2 sidenav">
                </div>
                <!--Contingut pagina -->
                <div class="col-sm-8 text-left"> 
                    <section class='row'>
                        <div class="col-md-12">
                            <h1>Log out</h1>
                            <p>Ha sido desconectado del servicio</p>
                            <hr />
                            <div align="center">
                                <img src="img/happy-employees-300.png" alt="employees" class="img-thumbnail">
                            </div>
                        </div>
                    </section>
                    <section class='row'>
                        <div class="col-md-6" align="center">
                            <h3>Volver a logear</h3>
                            <a href="./login.jsp" class="btn btn-default btn-lg">Dashboard</a>
                        </div>
                        <div class="col-md-6" align="center">
                            <h3>Volver a pagina principal</h3>
                            <a href="index.jsp" class="btn btn-default btn-lg">Employees</a>
                        </div>
                    </section>
                </div>
                <div class="col-sm-2 sidenav">
                </div>
            </div>
        </div>

        <footer class="container-fluid text-center">
           <div id="footer">
                <p>Empleados SL</p>
            </div>
        </footer>
    </body>
</html>
