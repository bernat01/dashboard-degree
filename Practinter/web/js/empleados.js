/**
 * Actualitza la informació del panell dret de l'empleat
 * @param {type} data
 * @returns {undefined}
 */
function emp_updateInfo(data) {
   var result = ws_parseResult(data);
    //console.log(result);
    var xml = result,
            xmlDoc = $.parseXML(xml),
            $xml = $(xmlDoc);
    var name = $xml.find("first_name").text();
    var last_name = $xml.find("last_name").text();
    var hire_date = $xml.find("hire_date").text();
    var birth_date = $xml.find("birth_date").text();
    $("#Employee-name").html(name);
    $("#Employee-last_name").html(last_name);
    $("#Employee-hire_date").html(hire_date);
    $("#Employee-birth_date").html(birth_date);
}

/**
 * Afegeix a la llista l'opció per carregar més empleats
 * @returns {undefined}
 */
function addOptionMore() {
    $("#selEmp").append('<option class="addEmps btn-link">más empleados</option>');
    $(".addEmps").click(function () {
        $(this).html('<img src="./img/ajax-loader.gif" width="220" height="19" />');
        var st = $("#selEmp").scrollTop(); //posició de l'scroll
        var startOption = parseInt($("#selEmp").find("option").length);
        var num = 30;

        console.log("values queried" + startOption + "-" + num);
        carregarEmpleats(startOption, num, st);
    });
}

function gen_graph_employee(no_emp, nombre_empleado) {
    ws_getSalaryEmployee(no_emp, function (data, status) {
        var result = ws_parseResult(data);
        //console.log(result);
        var xml = result,
                xmlDoc = $.parseXML(xml),
                $xml = $(xmlDoc);
        var eixX = [];
        $xml.find("from_date").each(function (index) {
            //console.log($(this).text());
            eixX.push($.trim($(this).text().toString()));
        });
        var eixY = [];
        var serie = {};
        serie.name = nombre_empleado;
        serie.data = [];
        $xml.find("salary").each(function (index) {
            serie.data.push(parseInt($(this).text()));
        });
        eixY[0] = serie;
        genLine('#EmployeeChart', 'Evolución salarial: ' + nombre_empleado, 'dolars anuals', 'dolars($)', eixX, eixY);
    });
}

function emp_initialList(){
    var n_employees = 31;
    //sessionStorage.clear();
    //empleats inicials de la llista
    if (typeof(Storage) !== "undefined") {
        console.log("1");
        if(localStorage.firstEmployees){//empleats carregats a memoria local
            console.log(localStorage.firstEmployees);
            var employees = localStorage.firstEmployees.split(",");
            for(var i in employees){
                $("#selEmp").append("<option>" + employees[i] + "</option>");
            }
            addOptionMore();
        }else{
            console.log("3");
            ws_getRangeEmployees(1, n_employees, function (data, status) {
                var result = ws_parseResult(data);
                //console.log(result);
                var xml = result,
                        xmlDoc = $.parseXML(xml),
                        $xml = $(xmlDoc);
                var employees=[];
                $xml.find("empleado").each(function (index) {
                    var name = $(this).find("first_name");
                    var emp_no = $(this).find("emp_no");
                    var empl = emp_no.text().trim() + "-" + name.text().trim();
                    employees.push(empl);
                });
                localStorage.firstEmployees = employees;
                for(var i in employees){
                    $("#selEmp").append("<option>" + employees[i] + "</option>");
                }
                addOptionMore();

            });
        }
    } else {
        console.log("4");
        // Sorry! No Web Storage support..
        carregarEmpleats(1, n_employees, 0);
    }
}

/**
 * 
 * @param {type} numPrimer
 * @param {type} nombre
 * @param {type} st Posició on volem s'scroll
 * @returns {undefined}
 */
function carregarEmpleats(numPrimer, nombre, st) {
    ws_getRangeEmployees(numPrimer, nombre, function (data, status) {
        var result = ws_parseResult(data);
        //console.log(result);
        var xml = result,
                xmlDoc = $.parseXML(xml),
                $xml = $(xmlDoc);

        $xml.find("empleado").each(function (index) {
            var name = $(this).find("first_name");
            var emp_no = $(this).find("emp_no");
            var empl = emp_no.text() + "-" + name.text();
            //console.log(empl)
            $("#selEmp").append("<option>" + empl + "</option>");
        });
        $(".addEmps").remove();
        addOptionMore();
        $("#loadingEmp").hide();
        $("#selEmp").scrollTop(st); //posició de l'scroll
    });
}


$(document).ready(function () {
    //genBar('#AgeBar', 'Trabajadores según año de nacimiento', categories, data);
    //gen_graph_employee(10001, "primero");
    $("#Employee-info").hide();
    
    emp_initialList();

    $("#selEmp").change(function () {
        var empl = $("#selEmp").val();
        if (empl != "más empleados") {
            var optData = empl.split("-");
            var no_emp = parseInt(optData[0]);
            gen_graph_employee(no_emp, optData[1]);
            console.log(no_emp - 10000);    
            $("#Employee-info").prop( "disabled", true );
            ws_getEmployee(no_emp - 10000, function (data, status) {             
                emp_updateInfo(data)
                $("#Employee-info").show();
            });
        }
    });
});