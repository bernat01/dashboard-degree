
/**
 * Carrega el chart com un drilldown amb els porcentatges indicats
 * @param {type} chart
 * @param {type} point
 * @param {type} data con el formato[[key, value],[key, value],...]
 * @returns {undefined}
 */
function hgc_loadDrillDown(chart, point, data) {
    console.log(point);
    var series = {
        name: point.name,
        id: point.id,
        data: data
    };
    chart.hideLoading();
    chart.addSeriesAsDrilldown(point, series);
}


function hgc_buidSeriesPie(data_name, json_data) {
    var series = [{
            name: data_name,
            colorByPoint: true,
            data: json_data
        }];
    return series;
}

/**
 * 
 * @param {type} place
 * @param {type} title
 * @param {type} name_data
 * @param {type} json_data
 * @returns {undefined}
 */
function genPie(place, title, name_data, json_data) {
    $(place).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: title
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
                name: name_data,
                colorByPoint: true,
                data: json_data
            }]
    });
}

/**
 * 
 * @param {type} place
 * @param {type} title
 * @param {type} subtitle
 * @param {type} json_series
 * @param {type} drillDownEvent
 * @returns {undefined}
 */
function genPie_drilldown(place, title, subtitle, json_series, drillDownEvent) {
    $(place).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            events: {
                drilldown: drillDownEvent
            }

        },
        title: {
            text: title
        },
        subtitle: {
            text: subtitle
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        series: json_series
    });
}

function genBar(place, title, json_categories, json_series) {
    $(place).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        xAxis: {
            categories: json_categories
        },
        credits: {
            enabled: false
        },
        series: json_series,
        drilldown: {
            series: []
        }
    });
}

function genLine(place, title, subtitle, type_values, json_categories, json_series) {
    $(place).highcharts({
        title: {
            text: title,
            x: -20 //center
        },
        subtitle: {
            text: subtitle,
            x: -20
        },
        xAxis: {
            categories: json_categories
        },
        yAxis: {
            title: {
                text: type_values
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        tooltip: {
            valueSuffix: '$'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: json_series
    });
}
