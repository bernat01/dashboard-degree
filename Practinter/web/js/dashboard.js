var refreshPrioridad, refreshRiesgo;


function loadContent(place, url) {
    $(place).html('<img src="./img/ajax-loader.gif" width="220" height="19" />');
    $(place).load(url);
}

$(document).ready(function () {
    $(".menuItem").click(function () {
        var num_item = $(this).index();
        //alert("Index: " + num_item);
        $('.nav-sidebar li.active').removeClass('active');
        $(this).addClass('active');

        switch (num_item) {
            case 0:
                $("#sectName").html("Departamentos");
                loadContent('#panelPrincipal', 'includes/departamentos.jsp');
                break;
            case 1:
                $("#sectName").html("Empleados");
                loadContent('#panelPrincipal', 'includes/employees.jsp');
                break;
            case 2:
                $("#sectName").html("Salarios");
                loadContent('#panelPrincipal', 'includes/salarios.jsp');
                break;
            case 3:
                break;
        }
    });

    //initial config
    ws_putNumEmpleados("#nEmpl");
    $("#sectName").html("Departamentos");
    $('#panelPrincipal').load('includes/departamentos.jsp');

    

});



/*
 $(function () {
 $('#panelPrincipal').load('./PHP/showProyectos.php');
 });
 
 function updateLeftMenu(item) {
 var location = item.split('-');
 $('.nav-sidebar li.active').removeClass('active');
 $('#menu' + location[0]).addClass('active');
 
 switch (location[0]) {
 case '1':
 $('#panelPrincipal').load('./PHP/showProyectos.php');
 break;
 case '2':
 if (location.length == 2) {
 if (location[1] == 1)
 $('#panelPrincipal').load('./PHP/showPropuestas.php');
 else
 $('#panelPrincipal').load('./PHP/addPropuesta.php');
 }
 break;
 case '3':
 $('#panelPrincipal').load('./PHP/showMetricas.php');
 break;
 case '4':
 $('#panelPrincipal').load('./PHP/showPrincipios.php');
 break;
 case '5':
 $('#panelPrincipal').load('./PHP/showEvaluaciones.php');
 break;
 case '6':
 $('#panelPrincipal').load('./PHP/showEconomia.php');
 break;
 case '7':
 $('#panelPrincipal').load('./PHP/showHumanos.php');
 break;
 }
 }
 
 $(document).ajaxComplete(function (event, xhr, settings) {
 switch (settings.url) {
 case './PHP/showEconomia.php':
 $('#tablaEconomia').tablesorter();
 break;
 case './PHP/showProyectos.php':
 $('#tablaProyectos').tablesorter();
 loadObjetivoSelect();
 break;
 case './PHP/showPropuestas.php':
 $('#tablaPropuestas').tablesorter();
 loadObjetivoSelect();
 
 $('.cambioEstado').click(function () {
 $('#cambiarEstadoPropuesta').removeClass('disabled');
 $('.cambioEstado').removeClass('active');
 $(this).addClass('active');
 
 switch ($(this).attr('id')) {
 case 'cambioEstado1':
 $('#nuevoEstadoPropuesta').val('1');
 break;
 case 'cambioEstado2':
 $('#nuevoEstadoPropuesta').val('2');
 break;
 case 'cambioEstado3':
 $('#nuevoEstadoPropuesta').val('3');
 break;
 }
 });
 break;
 case './PHP/addPropuesta.php':
 loadElementsAfterAjax();
 break;
 }
 
 if (settings.url.indexOf('./PHP/showProyecto.php?id=') > -1) {
 loadElementsAfterAjax();
 $('div h3.info-project-title').addClass('is-in');
 } else if (settings.url.indexOf('./PHP/showEvaluacion.php?id=') > -1) {
 $('#getFecha').change(function () {
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 0,
 proyecto: $('#proyectoParaEvaluacion').val(),
 value: $(this).val(),
 },
 success: function (data) {
 var num = data.split(",");
 var chart = $('#highEvaluacion').highcharts();
 chart.series[0].setData([parseInt(num[0]), parseInt(num[1]), parseInt(num[2]), parseInt(num[3]), parseInt(num[4]), parseInt(num[5])]);
 $('#comentarioEvaluacion').html(num[6]);
 }
 });
 });
 }
 ;
 
 });
 
 function loadElementsAfterAjax() {
 refreshPrioridad, refreshRiesgo = false;
 loadCalendar();
 loadSlider();
 }
 
 function showProyecto(idProyecto) {
 $('.nav-sidebar li.active').removeClass('active');
 $('#menu1').addClass('active');
 $('#panelPrincipal').load('./PHP/showProyecto.php?id=' + idProyecto);
 }
 
 function showPropuesta(idProyecto) {
 $('#panelPrincipal').load('./PHP/showProyecto.php?id=' + idProyecto);
 }
 
 function showMetrica(idMetrica) {
 $('#metrica').load('./PHP/showMetrica.php?id=' + idMetrica);
 setTimeout(function () {
 $('html body').animate({scrollTop: $(document).height()}, 1000);
 }, 100);
 }
 
 function showObjetivos(idPrincipio) {
 $('#objetivos').hide().load('./PHP/showObjetivos.php?id=' + idPrincipio).fadeIn('fast');
 $('#botonAddObjetivo').removeClass('hidden');
 $('#idPrincipio').val(idPrincipio);
 }
 
 function showEvaluacion(idProyecto) {
 $('#proyectoParaEvaluacion').val(idProyecto);
 $('#evaluacionModal').load('./PHP/showEvaluacion.php?id=' + idProyecto);
 }
 
 function showProyectosObjetivos(idProyectoObjetivo) {
 updateLeftMenu('1');
 setTimeout(function () {
 $("#filtroObjetivos").val('' + idProyectoObjetivo + '').trigger('change');
 $('html, body').animate({scrollTop: $("#tablaProyectos").offset().top}, 2000);
 }, 500);
 }
 
 function counterChar(val, counter) {
 var len = val.value.length;
 if (len >= 256)
 val.value = val.value.substring(0, 256);
 else
 $(counter).html(256 - len);
 }
 
 function filtrarProyectosObjetivos(idObjetivo) {
 $('.objetivos').closest('tr').hide();
 
 if (idObjetivo == 0)
 $('#tablaProyectos td').closest('tr').show();
 else
 $('.objetivo' + idObjetivo).closest('tr').show();
 }
 
 function filtrarProyectosAlertas(idAlerta) {
 $('.objetivos').closest('tr').hide();
 
 if (idAlerta == 0)
 $('#tablaProyectos td').closest('tr').show();
 else if (idAlerta == 1)
 $('.danger').closest('tr').show();
 else if (idAlerta == 2)
 $('.warning').closest('tr').show();
 else
 $('.success').closest('tr').show();
 }
 
 function filtrarPropuestasEstado(idAlerta) {
 $('.estados').closest('tr').hide();
 
 if (idAlerta == 0)
 $('#tablaPropuestas td').closest('tr').show();
 else if (idAlerta == 1)
 $('.estado1').closest('tr').show();
 else if (idAlerta == 2)
 $('.estado2').closest('tr').show();
 else
 $('.estado3').closest('tr').show();
 }
 
 function cargarModalCambioEstado(estadoActual, idPropuesta) {
 $('.cambioEstado').removeClass('active disabled');
 
 switch (estadoActual) {
 case 1:
 $('#cambioEstado1').addClass('active disabled');
 break;
 case 2:
 $('#cambioEstado2').addClass('active disabled');
 break;
 case 3:
 $('#cambioEstado3').addClass('active disabled');
 break;
 }
 
 $('#idPropuestaEstadoHidden').val(idPropuesta);
 }
 
 function cargarModalEconomia(idProyecto) {
 $('#graficosProyectoEconomia').load('./PHP/infoEconomiaProyecto.php?id=' + idProyecto);
 }
 
 function cargarModalAceptarPropuesta(idPropuesta) {
 $('#idPropuestaHidden').val(idPropuesta);
 }
 
 function cargarModalModificarPrincipio(idPrincipio) {
 $('#idPrincipioModificar').val(idPrincipio);
 }
 
 function cargarModalModificarObjetivo(idObjetivo) {
 $('#idObjetivoModificar').val(idObjetivo);
 }
 
 function cargarModalBorrarPrincipio(idPrincipio) {
 $('#idPrincipioBorrar').val(idPrincipio);
 }
 
 function cargarModalBorrarObjetivo(idObjetivo) {
 $('#idObjetivoBorrar').val(idObjetivo);
 }
 
 function cargarModalModificarProyectoMetrica(idProyecto) {
 $('#idProyectoMetrica').val(idProyecto);
 }
 
 function cargarModalBorrarMetrica(idMetrica) {
 $('#idMetricaBorrar').val(idMetrica);
 }
 
 function cargarModalActualizarMetricaProyecto(idMetrica) {
 $('#idModificarMetrica').val(idMetrica);
 }
 
 function cargarModalBorrarMetricaProyecto(idMetrica) {
 $('#idMetricaProyectoBorrar').val(idMetrica);
 }
 
 function cargarModalRecursosProyecto(idProyecto) {
 $('#newUpdateRecursos').val(idProyecto);
 }
 
 function cargarModifyMetrica(idMetrica) {
 $('#metricaGeneral').val(idMetrica);
 }
 
 function cargarModalEvaluacion(idProyecto) {
 $('#proyectoParaEvaluacion').val(idProyecto);
 }
 
 function updatePresupuesto() {
 if (!(parseInt($('#valorPresupuesto').val()) > 0)) {
 feedBackModal('-1/Indica un número positivo, por favor.');
 return 0;
 }
 
 $('#cambiarPresupuesto').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 1,
 presupuesto: $('#valorPresupuesto').val()
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showEconomia.php');
 }, 500);
 }
 });
 }
 
 function updateRecursos() {
 if (!(parseInt($('#valorRecursos').val()) > 0)) {
 feedBackModal('-1/Indica un número positivo, por favor.');
 return 0;
 }
 
 $('#cambiarActivos').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 18,
 recursos: $('#valorRecursos').val()
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showHumanos.php');
 }, 500);
 }
 });
 }
 
 function updateRecursosProyecto() {
 if (!(parseInt($('#valorRecursosProyecto').val()) > 0)) {
 feedBackModal('-1/Indica un número positivo, por favor.');
 return 0;
 }
 
 $('#updateRecursosProyecto').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 19,
 proyecto: $('#newUpdateRecursos').val(),
 recursos: $('#valorRecursosProyecto').val()
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showHumanos.php');
 }, 500);
 }
 });
 }
 
 function updateInfoProyecto(idProyecto) {
 $('#updateProyecto').modal('hide');
 
 var datos = {
 'type': 2,
 'updateProyectoObjetivo': $('#updateProyectoObjetivo').val(),
 'inputNombre': $('#inputNombre').val(),
 'inputDescripcion': $('#inputDescripcion').val(),
 'inputFecha': $('#fechaUpdate').val() + ' 00:00:00',
 'inputCoste': $('#inputCoste').val(),
 'inputPresupuesto': $('#inputPresupuesto').val(),
 'inputCoste1': $('#inputCoste1').val(),
 'inputCoste3': $('#inputCoste3').val(),
 'inputResponsable': $('#inputResponsable').val(),
 'inputID': idProyecto
 };
 datos.inputPrioridad = (refreshPrioridad == true ? $('#prioridadValue').val() : '');
 datos.inputRiesgo = (refreshRiesgo == true ? $('#riesgoValue').val() : '');
 
 $.ajax({
 url: './PHP/process_functions.php',
 data: datos,
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showProyecto.php?id=' + idProyecto);
 }, 500);
 }
 });
 }
 
 function createPropuesta() {
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 3,
 insertPropuestaObjetivo: $('#insertPropuestaObjetivo').val(),
 inputNombre: $('#inputNombre').val(),
 inputDescripcion: $('#inputDescripcion').val(),
 inputCoste: $('#inputCoste').val(),
 inputPresupuesto: $('#inputPresupuesto').val(),
 inputCoste1: $('#inputCoste1').val(),
 inputCoste2: $('#inputCoste2').val(),
 inputCoste3: $('#inputCoste3').val(),
 inputPrioridad: $("#prioridadValue").val(),
 inputRiesgo: $("#riesgoValue").val()
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showPropuestas.php');
 }, 500);
 }
 });
 }
 
 function crearEvaluacion() {
 $('#addEvaluacion').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 4,
 inputProyecto: $('#inputProyecto').val(),
 Conducta: $('#inputConducta').val(),
 Responsabilidad: $('#inputResponsabilidad').val(),
 Estrategia: $('#inputEstrategia').val(),
 Adquisicion: $('#inputAdquisicion').val(),
 Rendimiento: $('#inputRendimiento').val(),
 Conformidad: $('#inputConformidad').val(),
 comentario: $('#inputComment').val(),
 },
 success: function (data) {
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showEvaluaciones.php');
 }, 500);
 feedBackModal(data);
 }
 });
 }
 
 function aceptarPropuesta() {
 $('#aceptarPropuesta').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 5,
 idPropuestaProyecto: $('#idPropuestaHidden').val(),
 responsable: $('#inputNombre').val(),
 motivo: $('#motivoPropuesta').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showProyecto.php?id=' + $('#idPropuestaHidden').val());
 }, 500);
 }
 });
 }
 
 function cambiarEstadoPropuesta() {
 $('#cambiarEstado').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 6,
 idPropuestaProyecto: $('#idPropuestaEstadoHidden').val(),
 nuevoEstado: $('#nuevoEstadoPropuesta').val(),
 motivo: $('#motivoPropuesta').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showPropuestas.php');
 }, 500);
 }
 });
 }
 
 function insertarPrincipio() {
 if ($('#inputNuevoNombre').val() == '') {
 feedBackModal('-1/Indique un nombre en el campo, por favor.');
 return 0;
 }
 
 $('#nuevoPrincipio').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 7,
 nombre: $('#inputNuevoNombre').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showPrincipios.php').val();
 }, 500);
 }
 });
 }
 
 function insertarObjetivo() {
 if ($('#inputNuevoObjetivo').val() == '') {
 feedBackModal('-1/Indique un nombre en el campo, por favor.');
 return 0;
 }
 
 $('#nuevoObjetivo').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 8,
 principio: $('#nuevoObjetivoPrincipio').val(),
 nombre: $('#inputNuevoObjetivo').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showPrincipios.php').val();
 }, 500);
 }
 });
 }
 
 function updateInfoObjetivo() {
 if ($('#inputActualizarNombreObjetivo').val() == '') {
 feedBackModal('-1/Indique un nombre en el campo, por favor.');
 return 0;
 }
 
 $('#modificarObjetivo').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 9,
 id: $('#idObjetivoModificar').val(),
 updateObjetivoPrincipio: $('#updateObjetivoPrincipio').val(),
 inputNombre: $('#inputActualizarNombreObjetivo').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showPrincipios.php');
 }, 500);
 }
 });
 }
 
 function updatePrincipio() {
 if ($('#inputActualizarNombrePrincipio').val() == '') {
 feedBackModal('-1/Indique un nombre en el campo, por favor.');
 return 0;
 }
 
 $('#modificarPrincipio').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 10,
 id: $('#idPrincipioModificar').val(),
 updatePrincipioName: $('#inputActualizarNombrePrincipio').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showPrincipios.php');
 }, 500);
 }
 });
 }
 
 function borrarPrincipio() {
 $('#borrarPrincipio').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 11,
 id: $('#idPrincipioBorrar').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showPrincipios.php');
 }, 500);
 }
 });
 }
 
 function borrarObjetivo() {
 $('#borrarObjetivo').modal('hide');
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 12,
 id: $('#idObjetivoBorrar').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showPrincipios.php');
 }, 500);
 }
 });
 }
 
 function crearMetrica() {
 if ($('#nameMetrica').val() == '') {
 feedBackModal('-1/Indique un nombre en el campo, por favor.');
 return 0;
 }
 
 $('#addMetrica').modal('hide');
 
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 13,
 nameMetrica: $('#nameMetrica').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showMetricas.php');
 }, 500);
 }
 });
 }
 
 function modifyMetrica(idMetrica) {
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 14,
 proyecto: $('#inputProyectoMetrica').val(),
 umbralMin: $('#inputUmbralMin').val(),
 umbralMax: $('#inputUmbralMax').val(),
 value: $('#inputValue').val(),
 metrica: $('#metricaGeneral').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showMetricas.php');
 }, 500);
 }
 });
 }
 
 function borrarProyectoMetrica() {
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 15,
 idProyectoMetrica: $('#idMetricaProyectoBorrar').val()
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showMetricas.php');
 }, 500);
 }
 });
 }
 
 function modificarMetricaProyecto() {
 if ($('#modifyUmbralMin').val() == '') {
 feedBackModal('-1/Indique un valor en los umbrales, por favor.');
 return 0;
 }
 
 $('#modificarProyectoMetrica').modal('hide');
 
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 16,
 idMetrica: $('#idProyectoMetrica').val(),
 umbralMin: $('#modifyUmbralMin').val(),
 umbralMax: $('#modifyUmbralMax').val(),
 value: $('#modifyValue').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showMetricas.php');
 }, 500);
 }
 });
 }
 
 function borrarMetrica() {
 $.ajax({
 url: './PHP/process_functions.php',
 data: {
 type: 17,
 metrica: $('#idMetricaBorrar').val(),
 },
 success: function (data) {
 if (feedBackModal(data))
 setTimeout(function () {
 $('#panelPrincipal').load('./PHP/showMetricas.php');
 }, 500);
 }
 });
 }
 
 function feedBackModal(message) {
 var mensaje = message.split('/');
 
 if (mensaje[0] == '-1') {
 //update modal content and show it
 $('#feedbackMessageError').html(mensaje[mensaje.length - 1]);
 $('#modalFeedbackError').modal('show');
 setTimeout(function () {
 $('#modalFeedbackError').modal('hide');
 }, 1500);
 } else {
 //update modal content and show it
 $('#feedbackMessage').html(mensaje[mensaje.length - 1]);
 $('#modalFeedback').modal('show');
 setTimeout(function () {
 $('#modalFeedback').modal('hide');
 }, 2500);
 
 //create and prepend new child to notification menu
 var salida = '';
 switch (mensaje[0]) {
 case '1':
 if (mensaje.length == 2)
 salida = '<a onclick="updateLeftMenu(\'1\')"><div><i class="fa fa-file-text"> </i> ';
 else
 salida = '<a onclick="showProyecto(\'' + mensaje[1] + '\')"><div><i class="fa fa-file-text"> </i> ';
 break;
 case '2':
 salida = '<a onclick="updateLeftMenu(\'2-1\')"><div><i class="fa fa-lightbulb-o"> </i> ';
 break;
 case '3':
 salida = '<a onclick="updateLeftMenu(\'3\')"><div><i class="fa fa-tachometer"> </i> ';
 break;
 case '4':
 salida = '<a onclick="updateLeftMenu(\'4\')"><div><span class="glyphicon glyphicon-screenshot"></span> </span>';
 break;
 case '6':
 salida = '<a onclick="updateLeftMenu(\'6\')"><div><i class="fa fa-money"> </i>';
 break;
 default:
 return true;
 break;
 }
 
 $('#notificationBadge').html(parseInt($('#notificationBadge').html()) + 1);
 $('#notificationsNavbar').prepend('</li><li>' + salida + ' ' + mensaje[mensaje.length - 1] + '</div></a></li><li role="separator" class="divider">');
 return true;
 }
 }
 
 function showPrioridad(valor) {
 $('#valorUpdateProyectoPrioridad').html(valor);
 }
 
 function showRiesgo(valor) {
 $('#valorUpdateProyectoRiesgo').html(valor);
 }
 
 function loadSlider() {
 $("#inputRiesgo").slider({
 value: 3,
 max: 5,
 min: 1,
 orientation: "horizontal",
 range: "min",
 animate: true,
 slide: function (event, ui) {
 refreshRiesgo = true;
 $("#riesgoValue").val(ui.value);
 }
 });
 $("#riesgoValue").val($("#inputRiesgo").slider("value"));
 
 $("#inputPrioridad").slider({
 value: 3,
 max: 5,
 min: 1,
 orientation: "horizontal",
 range: "min",
 animate: true,
 slide: function (event, ui) {
 refreshPrioridad = true;
 $("#prioridadValue").val(ui.value);
 }
 });
 $("#prioridadValue").val($("#inputPrioridad").slider("value"));
 }
 
 function loadCalendar() {
 $("#fechaUpdate").datepicker({
 firstDay: 1,
 dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
 monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"],
 dateFormat: "yy-mm-dd",
 buttonImage: "./img/calendar.gif",
 showOn: "button",
 buttonImageOnly: true,
 buttonText: "Selecciona la fecha",
 });
 }
 
 function loadObjetivoSelect() {
 setTimeout(function () {
 $('#filtroObjetivos').attr('disabled', false);
 }, 1000);
 
 $('#filtroObjetivos').on('change', function () {
 filtrarProyectosObjetivos(this.value);
 });
 }
 
 function inputEvaluacionesValidator(formInput) {
 var len = formInput.value.length;
 if (len >= 2)
 formInput.value = formInput.value.substring(0, 2);
 if (formInput.value != '' && !(parseInt(formInput.value) <= 10 && parseInt(formInput.value) > 0))
 formInput.value = '';
 }*/