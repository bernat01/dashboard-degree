
function formatData_genderDD(porc_males, porc_females) {
    return [
        ['Male', porc_males],
        ['Female', porc_females]
    ];
}


//event quan apretam sobre algun element del chart
var dd_event = function (e) {
    if (!e.seriesOptions) {
        var id_dept = e.point.id;
        console.log(id_dept);
        var chart = this;

        function ws_responsed(data, status) {
            var result = ws_parseResult(data);
            // console.log(result);
            var xml = result,
                    xmlDoc = $.parseXML(xml),
                    $xml = $(xmlDoc),
                    $porc_males = $xml.find("porc_males"),
                    $porc_females = $xml.find("porc_females");
            var porc_males = parseFloat($porc_males.text());
            var porc_females = parseFloat($porc_females.text());

            var dataStore = [porc_males, porc_females];
            sessionStorage[id_dept] = dataStore;
            
            var data_dd = formatData_genderDD(porc_males, porc_females)
            hgc_loadDrillDown(chart, e.point, data_dd);
        }
        // Show the loading label
        chart.showLoading('Loading data... ');

        if (typeof (Storage) !== "undefined") {
            if (sessionStorage[id_dept]) {//informació del departament enrregitrada
                var data = sessionStorage[id_dept];
                var porcs = data.split(",");
                var porc_males = parseFloat(porcs[0]);
                var porc_females = parseFloat(porcs[1]);
                
                var data_dd = formatData_genderDD(porc_males, porc_females)
                hgc_loadDrillDown(chart, e.point, data_dd);

            } else {
                console.log(2);
                //codi que s'ha d'executar despres de que haguin arribat les dades
                ws_getDeptGenderPorc(e.point.id, ws_responsed);
            }
        } else {
            console.log(3);
            // Sorry! No Web Storage support..
            ws_getDeptGenderPorc(e.point.id, ws_responsed);
        }
    }
}

var title = 'Distribución de los empleados entre los departamentos';
var subtitle = "Presiona sobre un departamento para ver la destribución por sexo";

//esperam que s'hagui acabat de carregar el document per afegir el chart
$(document).ready(function () {
    //series -> generat amb jsp
    genPie_drilldown('#Departments', title, subtitle, series, dd_event);
});

