/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var webServiceURL = 'http://localhost:8080/Practinter/WSAccesoABD';
var soapMessagegetnumempleados = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><S:Body><ns2:getNumEmpleados xmlns:ns2="http://adiiubdws/"/></S:Body></S:Envelope>';


function basicAjax(soap, callback) {
    $.ajax({
        url: webServiceURL,
        type: "POST",
        dataType: "text",
        data: soap,
        contentType: "text/xml; charset=\"utf-8\"",
        success: callback,
        error: OnError
    });
}

function ws_parseResult(data) {
    var auxstr = data.toString() + "";
    xmlDoc = $.parseXML(auxstr),
            $xml = $(xmlDoc),
            $content = $xml.find("return");
    return $content.text();
}

function ws_getSoap_employee(indice){
    var ini = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><S:Body><ns2:getEmpleado xmlns:ns2="http://adiiubdws/"><indice>';
    var fi = '</indice></ns2:getEmpleado> </S:Body></S:Envelope>';
    return ini + indice + fi;
}
function ws_getSoap_DeptGenderPorc(dept_no) {
    var ini = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><S:Body><ns2:getGenderPorc xmlns:ns2="http://adiiubdws/"><dept_no>';
    var fi = '</dept_no></ns2:getGenderPorc></S:Body></S:Envelope>';
    console.log(ini + dept_no + fi);
    return ini + dept_no + fi;
}

function ws_getSoap_EmployeesRange(n_inf, n_sup) {
    var ini = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/>    <S:Body>        <ns2:getEmpleados xmlns:ns2="http://adiiubdws/">            <i>';
    var fi = '</j></ns2:getEmpleados></S:Body></S:Envelope>';
    var result = ini + n_inf + '</i><j>' + n_sup + fi;

    return result;
}

function ws_getSoap_SalaryEmployee(num_emp) {
    var ini = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">    <SOAP-ENV:Header/>    <S:Body>        <ns2:getSalaryEmploy xmlns:ns2="http://adiiubdws/">            <emp_no>'
    var fi = '</emp_no>        </ns2:getSalaryEmploy>    </S:Body></S:Envelope>';

    var result = ini + num_emp + fi;

    return result;
}

function ws_getSalaryEmployee(num_emp, callback) {
    var soap = ws_getSoap_SalaryEmployee(num_emp);
    basicAjax(soap, callback);
    return false;
}


function ws_getRangeEmployees(n_inf, n_sup, callback) {
    $.ajax({
        url: webServiceURL,
        type: "POST",
        dataType: "text",
        data: ws_getSoap_EmployeesRange(n_inf, n_sup),
        contentType: "text/xml; charset=\"utf-8\"",
        success: callback,
        error: OnError
    });
    return false;
}

function ws_getDeptGenderPorc(dept_no, callback) {
    $.ajax({
        url: webServiceURL,
        type: "POST",
        dataType: "text",
        data: ws_getSoap_DeptGenderPorc(dept_no),
        contentType: "text/xml; charset=\"utf-8\"",
        success: callback,
        error: OnError
    });
    return false;
}

function ws_putNumEmpleados(place) {

    $.ajax({
        url: webServiceURL,
        type: "POST",
        dataType: "text",
        data: soapMessagegetnumempleados,
        contentType: "text/xml; charset=\"utf-8\"",
        success: function (data, status) {
            var result = ws_parseResult(data);
            $(place).html(result);
        },
        error: OnError
    });
    return false;
}

function getNumEmpleadosWS()
{
    $.ajax({
        url: webServiceURL,
        type: "POST",
        dataType: "text",
        data: soapMessagegetnumempleados,
        contentType: "text/xml; charset=\"utf-8\"",
        success: OnSuccess,
        error: OnError
    });
    return false;
}

/**
 * 
 * @param {type} indice
 * @param {type} callback
 * @returns {Boolean}
 */
function ws_getEmployee(indice, callback)
{
    console.log(indice);
    var soap = ws_getSoap_employee(indice);
    console.log(soap);
    $.ajax({
        url: webServiceURL,
        type: "POST",
        dataType: "text",
        data: soap,
        contentType: 'text/xml; charset="utf-8"',
        success: callback,
        error: OnError
    });
    return false;
}

function OnSuccess(data, status)
{
    var auxstr = data.toString() + "";
    var ini = auxstr.search("<return>") + 8;
    var fin = auxstr.search("</return>");
    auxstr = auxstr.substring(ini, fin);
    alert(auxstr);
}

function OnError(request, status, error)
{
    alert('error: ' + error + '<br>request: ' + request + "<br>status: " + status);
}

$(document).ready(function () {
    jQuery.support.cors = true;
});
