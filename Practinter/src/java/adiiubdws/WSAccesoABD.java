/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adiiubdws;

import adiiubd.AccesoABD;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author mascport
 */
@WebService(serviceName = "WSAccesoABD")
public class WSAccesoABD {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getNumEmpleados")
    public String getNumEmpleados() {
        AccesoABD abd = new AccesoABD();
        return abd.getNumEmpleados();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getEmpleado")
    public String getEmpleado(@WebParam(name = "indice") String indice) {
        AccesoABD abd = new AccesoABD();
        return abd.getEmpleado(Integer.parseInt(indice));
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getEmpleados")
    public String getEmpleados(@WebParam(name = "i") String i, @WebParam(name = "j") String j) {
        AccesoABD abd = new AccesoABD();
        return abd.getEmpleados(Integer.parseInt(i),Integer.parseInt(j));
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getSalaryEmploy")
    public String getSalaryEmploy(@WebParam(name = "emp_no") String emp_no) {
        AccesoABD abd = new AccesoABD();
        return abd.getSalarioEmpleado(Integer.parseInt(emp_no));
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getGenderPorc")
    public String getGenderPorc(@WebParam(name = "dept_no") String dept_no) {
        AccesoABD abd = new AccesoABD();
        return abd.getGenderPorc(dept_no);
    }


}
