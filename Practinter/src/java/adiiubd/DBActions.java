/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adiiubd;

import Dashboard.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import xml.MakersXML;

/**
 *
 * @author mascport
 */
public class DBActions {

    protected String getNumEmployees() {
        String res = "NOP";
        DBConnection con = new DBConnection();
        try {
            con.open();
            Statement st = con.getConection().createStatement();
            ResultSet rs = st.executeQuery("select count(*) from employees;");
            rs.next();
            res = Integer.toString(rs.getInt(1));
            res = (new MakersXML()).xmlNumEmpleados(res);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return res;
    }

    protected String getEmp(int i) {
        String res = "";
        DBConnection con = new DBConnection();
        try {
            con.open();
            Statement st = con.getConection().createStatement();
            ResultSet rs = st.executeQuery("select * from employees;");
            rs.absolute(i);
            String[] aux = new String[6];
            aux[0] = Integer.toString(rs.getInt("emp_no"));
            aux[1] = (rs.getDate("birth_date")).toString();
            aux[2] = rs.getString("first_name");
            aux[3] = rs.getString("last_name");
            aux[4] = rs.getString("gender");
            aux[5] = (rs.getDate("hire_date")).toString();
            res = (new MakersXML()).xmlEmpleado(aux);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return res;
    }

    protected String getEmps(int i, int j) {
        ArrayList<String> res = new ArrayList<String>();
        DBConnection con = new DBConnection();
        try {
            con.open();
            Statement st = con.getConection().createStatement();
            ResultSet rs = st.executeQuery("select * from employees;");
            rs.absolute(i);
            int vaux = i + j;
            while (i < (vaux - 1)) {
                String[] aux = new String[6];
                aux[0] = Integer.toString(rs.getInt("emp_no"));
                aux[1] = (rs.getDate("birth_date")).toString();
                aux[2] = rs.getString("first_name");
                aux[3] = rs.getString("last_name");
                aux[4] = rs.getString("gender");
                aux[5] = (rs.getDate("hire_date")).toString();
                res.add((new MakersXML()).xmlEmpleadoSimple(aux));
                rs.next();
                i++;
            }
            String[] aux = new String[6];
            aux[0] = Integer.toString(rs.getInt("emp_no"));
            aux[1] = (rs.getDate("birth_date")).toString();
            aux[2] = rs.getString("first_name");
            aux[3] = rs.getString("last_name");
            aux[4] = rs.getString("gender");
            aux[5] = (rs.getDate("hire_date")).toString();
            res.add((new MakersXML()).xmlEmpleadoSimple(aux));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return (new MakersXML()).xmlNEmpleados(res);
    }

    protected String getSalaryEmploy(int emp_no) {
        ArrayList<String> res = new ArrayList<String>();
        DBConnection con = new DBConnection();
        try {
            con.open();
            Statement st = con.getConection().createStatement();
            String sql = "select * from salaries where emp_no = " + emp_no + " ORDER BY from_date ASC;";
            System.out.println(sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("salary"));
                res.add(Integer.toString(rs.getInt("salary")));
                res.add(rs.getDate("from_date").toString());
                res.add(rs.getDate("to_date").toString());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return (new MakersXML()).xmlSalaryEmploy(emp_no, res);
    }

    /**
     * Devuelve array list de tamaño del numero de departamentos En cada
     * posición tiene un vector de strings 
     *          [0] => dept_no 
     *          [1] => nombre_dept 
     *          [2] => n_emple
     *
     * @return
     */
    public ArrayList<String[]> getNumEmpByDept() {
        DBConnection con = new DBConnection();
        ArrayList<String[]> res = new ArrayList<String[]>();

        try {
            con.open();
            Statement st = con.getConection().createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT "
                    + "    departments.dept_no as dept_no, departments.dept_name as dept, COUNT(*) "
                    + "FROM"
                    + "    dept_emp INNER JOIN departments "
                    + "    ON dept_emp.dept_no = departments.dept_no "
                    + "GROUP BY departments.dept_no");
            while (rs.next()) {
                String[] r = new String[3];
                r[0] = rs.getString("dept_no");
                r[1] = rs.getString("dept");
                r[2] = Integer.toString(rs.getInt("COUNT(*)"));
                res.add(r);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return res;
    }

    /**
     * Consulta salario medio por año, así tenemos una estimación del salario
     * del año. Consulta muy pesada, mejor no usarla
     *
     * @return ArrayList de 2 stings [0] => año [1] => salario_medio
     */
    public ArrayList<String[]> getAvgSalByAge() {
        DBConnection con = new DBConnection();
        ArrayList<String[]> res = new ArrayList<String[]>();

        try {
            con.open();
            Statement st = con.getConection().createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT "
                    + "	AVG(salaries.salary), YEAR(salaries.from_date) as year "
                    + "FROM "
                    + "	salaries "
                    + "GROUP BY YEAR(salaries.from_date)");
            while (rs.next()) {
                String[] r = new String[2];
                r[0] = rs.getString("year");
                r[1] = Integer.toString(rs.getInt("AVG(salaries.salary)"));
                res.add(r);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return res;
    }

    public ArrayList<int[]> getNumEmpByAge() {
        DBConnection con = new DBConnection();
        ArrayList<int[]> res = new ArrayList<int[]>();

        try {
            con.open();
            Statement st = con.getConection().createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT "
                    + "	YEAR(employees.birth_date), COUNT(*) "
                    + "FROM"
                    + "	employees "
                    + "GROUP BY YEAR(employees.birth_date);");
            System.out.println("SELECT "
                    + "	YEAR(employees.birth_date), COUNT(*) "
                    + "FROM"
                    + "	employees "
                    + "GROUP BY YEAR(employees.birth_date);");
            while (rs.next()) {
                int[] r = new int[2];
                r[0] = rs.getInt("YEAR(employees.birth_date)");
                r[1] = rs.getInt("COUNT(*)");
                res.add(r);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return res;
    }

    public float[] getPorcBySex() {
        DBConnection con = new DBConnection();
        float[] res = new float[2];
        res[0] = 0; //male porc
        res[1] = 0; //female porc
        try {
            con.open();
            Statement st = con.getConection().createStatement();
            ResultSet rs = st.executeQuery("select count(*) from employees;");
            rs.next();
            float numEmp = rs.getInt(1);

            Statement st1 = con.getConection().createStatement();
            ResultSet rs1 = st1.executeQuery("select count(*) from employees group by gender;");
            rs1.first();
            float n_males = rs1.getInt("count(*)");
            rs1.next();
            int n_females = rs1.getInt("count(*)");

            res[0] = (n_males / numEmp)*100;
            res[1] = (n_females / numEmp)*100;
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return res;
    }

    

    /**
     *
     * @param dept_no
     * @return [0] => male porcentage [1] => femalePorc
     */
    public float[] getPorcBySex(String dept_no) {
        DBConnection con = new DBConnection();
        float[] res = new float[2];
        res[0] = 0; //male porc
        res[1] = 0; //female porc
        try {
            con.open();
            Statement st = con.getConection().createStatement();
            String ssql = "select count(*) from "
                    + "employees INNER JOIN dept_emp "
                    + "ON dept_emp.emp_no= employees.emp_no "
                    + "WHERE dept_emp.dept_no=\"" + dept_no + "\";";
            System.out.println(ssql);
            ResultSet rs = st.executeQuery(ssql);
            rs.next();
            float numEmp = rs.getInt(1);

            Statement st1 = con.getConection().createStatement();
            ssql = "select count(*) from "
                    + "employees INNER JOIN dept_emp "
                    + "ON dept_emp.emp_no= employees.emp_no "
                    + "WHERE dept_emp.dept_no=\"" + dept_no +"\""
                    + " group by gender;";
            System.out.println(ssql);
            ResultSet rs1 = st1.executeQuery(ssql);
            rs1.first();
            float n_males = rs1.getInt("count(*)");
            rs1.next();
            int n_females = rs1.getInt("count(*)");

            res[0] = (n_males / numEmp)*100;
            res[1] = (n_females / numEmp)*100;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }

        return res;
    }

  
    public String getGenderPorc() {
        float[] porcs = getPorcBySex();
        return (new MakersXML()).xmlGenderPorc(
                Float.toString(porcs[0]),//males
                Float.toString(porcs[1])); //females
    }
    
    public String getGenderPorc(String dept_no) {
        float[] porcs = getPorcBySex(dept_no);
        return (new MakersXML()).xmlGenderPorc(
                Float.toString(porcs[0]),//males
                Float.toString(porcs[1])); //females
    }

    public static boolean checkUser(User user) {
        DBConnection con = new DBConnection();

        try {
            con.open();
            Statement st = con.getConection().createStatement();
            String query = "select * from dashboard_users WHERE username ='" + user.getUsername() + "' "
                    + "AND password='" + user.getPassword() + "';";
            ResultSet rs = st.executeQuery(query);
            System.out.print(query);
            if (rs.first()) {//retorna fals si no hi ha resultat i true si s'ha posicionat sobre el primer
                return true;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            System.err.println(ex.getSQLState());
        } finally {
            con.close();
        }
        return false;
    }
}
