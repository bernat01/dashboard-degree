/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adiiubd;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author mascport
 */
public class AccesoABD implements Serializable {

    public String getNumEmpleados() {
        DBActions dba = new DBActions();
        return dba.getNumEmployees();
    }

    public String getEmpleado(int i) {
        DBActions dba = new DBActions();
        String res = dba.getEmp(i);
        return res;
    }

    public String getEmpleados(int i, int j) {
        DBActions dba = new DBActions();
        String res = dba.getEmps(i, j);
        return res;
    }

    public String getSalarioEmpleado(int i) {
        DBActions dba = new DBActions();
        String res = dba.getSalaryEmploy(i);
        return res;
    }

    public String getGenderPorc() {
        DBActions dba = new DBActions();
        return dba.getGenderPorc();
    }
    
    public String getGenderPorc(String dept_no) {
        DBActions dba = new DBActions();
        return dba.getGenderPorc(dept_no);
    }

    public ArrayList<int[]> getNumEmpByAge() {
        DBActions dba = new DBActions();
        return dba.getNumEmpByAge();
    }

    public ArrayList<String[]> getNumEmpByDept() {
        DBActions dba = new DBActions();
        return dba.getNumEmpByDept();
    }

    public ArrayList<String[]> getAvgSalByAge() {
        DBActions dba = new DBActions();
        return dba.getAvgSalByAge();
    }
}
