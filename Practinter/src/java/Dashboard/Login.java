/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dashboard;

import adiiubd.DBConnection;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author bernat
 */
public class Login  implements Serializable{

    private String username;
    private int id = -1;

    /**
     * Executa el login de l'usuari
     * @param req
     * @param resp
     * @return 
     *      0 -> no s'ha introduït cap dada encara
     *      1 -> login success
     *      2 -> login fail
     */
    public int doLogin(HttpServletRequest req, HttpServletResponse resp) {
        if (this.isLoggedIn()) {
            try {
                resp.sendRedirect("dashboard.jsp");
            } catch (IOException ex) {
            }//no es toca aixecar mai
        }

        String user = req.getParameter("IPusername");
        String pass = req.getParameter("IPPassword");
        try {
            
        
            if (user == null || pass == null) {
                return 0;
            }
            
            pass = toMD5(pass);
            
            if (this.check(user, pass)) {
                //redirect to mainPage
                try {
                    System.out.print("login success!");
                    resp.sendRedirect("dashboard.jsp");
                    return 1;//no s'executa
                } catch (IOException ex) {
                }//no es toca aixecar mai

            } else {
                return 2;
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 2;
    }

    public static String toMD5(String str) throws NoSuchAlgorithmException {
        MessageDigest m;
        m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(str.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);

        // Now we need to zero pad it if you actually want the full 32 chars.
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }
        return hashtext;
    }
    
    public void blindPage(HttpServletResponse resp) {
        if (!this.isLoggedIn()) {
            try {
                resp.sendRedirect("login.jsp");
            } catch (IOException ex) {
            }//no es toca aixecar mai 
        }
    }

    public boolean check(String username, String psw) {
        ArrayList<String> res = new ArrayList<String>();
        DBConnection con = new DBConnection();

        try {
            con.open();
            Statement st = con.getConection().createStatement();
            String query = "select * from dashboard_users WHERE username ='" + username + "' AND password='" + psw + "';";
            ResultSet rs = st.executeQuery(query);
            System.out.print(query);
            if (rs.first()) {//retorna fals si no hi ha resultat i true si s'ha posicionat sobre el primer
                this.username = rs.getNString("username");
                this.id = rs.getInt("id");
                return true;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            System.err.println(ex.getSQLState());
        } finally {
            con.close();
        }
        return false;
    }

    public boolean isLoggedIn() {
        return id != -1;
    }

    public void logout() {
        this.id = -1;
    }
}
